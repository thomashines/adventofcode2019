#!/usr/bin/env python3

import itertools
import sys

def position_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(parameter, 0)
  else:
    prog[parameter] = write

def immediate_mode(prog, parameter, write=None):
  if write is None:
    return parameter

MODES = [position_mode, immediate_mode]

def use_mode(mode_index, prog, parameter, write=None):
  return MODES[mode_index](prog, parameter, write=write)

def run_intcode(prog, input):
  output = []
  index = 0
  while index in prog:
    instr = "{:05}".format(prog[index])
    opcode = int(instr[-2:])
    modes = tuple(int(mode) for mode in instr[:3][::-1])
    params = tuple(prog.get(index + param + 1, 0) for param in range(3))
    print("index", index)
    print("instr", instr)
    print("modes", modes)
    if opcode == 1:
      # Add
      use_mode(modes[2], prog, params[2],
               write=(use_mode(modes[0], prog, params[0]) +
                      use_mode(modes[1], prog, params[1])))
      index += 4
    elif opcode == 2:
      # Multiply
      use_mode(modes[2], prog, params[2],
               write=(use_mode(modes[0], prog, params[0]) *
                      use_mode(modes[1], prog, params[1])))
      index += 4
    elif opcode == 3:
      # Input
      value = input.pop(0)
      print("input", value)
      use_mode(modes[0], prog, params[0], write=value)
      index += 2
    elif opcode == 4:
      # Output
      value = use_mode(modes[0], prog, params[0])
      print("output", value)
      output.append(value)
      index += 2
    elif opcode == 5:
      # Jump if true
      if use_mode(modes[0], prog, params[0]) == 0:
        index += 3
      else:
        index = use_mode(modes[1], prog, params[1])
    elif opcode == 6:
      # Jump if false
      if use_mode(modes[0], prog, params[0]) == 0:
        index = use_mode(modes[1], prog, params[1])
      else:
        index += 3
    elif opcode == 7:
      # Less than
      value = 1 if (use_mode(modes[0], prog, params[0]) <
                    use_mode(modes[1], prog, params[1])) else 0
      use_mode(modes[2], prog, params[2], write=value)
      index += 4
    elif opcode == 8:
      # Equals
      value = 1 if (use_mode(modes[0], prog, params[0]) ==
                    use_mode(modes[1], prog, params[1])) else 0
      use_mode(modes[2], prog, params[2], write=value)
      index += 4
    elif opcode == 99:
      # Exit
      break
  return output

if __name__ == "__main__":
  prog_string = ",".join(line.strip() for line in sys.stdin)
  orig_prog = {key: value for key, value in zip(itertools.count(), (int(val) for val in prog_string.split(",")))}
  outputs = []
  for input in [[1], [5]]:
    outputs.append(run_intcode(dict(orig_prog), input=input))
  print("outputs", outputs)
