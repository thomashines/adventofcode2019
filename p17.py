#!/usr/bin/env python3

import collections
import itertools

import p

import intcode

CARDINALS = (
  (+1, 0), # E
  (0, -1), # N
  (-1, 0), # W
  (0, +1), # S
)

@p.puzzle(17, 1)
def main(lines):
  program = intcode.IntCode(dict(enumerate(map(int, lines[0].split(",")))))

  scaffolds = set()

  program.run()
  start = None
  x = 0
  y = 0
  while program.has_output():
    character = chr(program.output())
    if character == "\n":
      x = 0
      y += 1
    elif character == ".":
      x += 1
    elif character == "#":
      scaffolds.add((x, y))
      x += 1
    elif character in "<^v>":
      start = ((x, y), dict(zip("<^v>", CARDINALS))[character])
      scaffolds.add((x, y))
      x += 1
    # print(character, end="")

  size = (
    max(x for x, _ in scaffolds) + 1,
    max(y for _, y in scaffolds) + 1,
  )

  corners = set()
  ends = set()
  intersections = set()
  for x, y in scaffolds:
    cardinals = tuple(((x + dx, y + dy) in scaffolds) for dx, dy in CARDINALS)
    if cardinals.count(True) == 1:
      ends.add((x, y))
    elif cardinals.count(True) == 2:
      if cardinals not in ((True, False, True, False), (False, True, False, True)):
        corners.add((x, y))
    elif cardinals.count(True) in (3, 4):
      intersections.add((x, y))

  yield sum(x * y for x, y in intersections)

  vertices = corners | ends
  edges = collections.defaultdict(lambda: {})
  for v0 in vertices:
    for v1 in vertices:
      if v0 == v1 or (v0[0] != v1[0] and v0[1] != v1[1]):
        continue
      dx = 0 if (v0[0] == v1[0]) else (1 if (v0[0] < v1[0]) else -1)
      dy = 0 if (v0[1] == v1[1]) else (1 if (v0[1] < v1[1]) else -1)
      valid = True
      x, y = v0
      while valid and ((x, y) != v1):
        valid = (x, y) in scaffolds
        x, y = x + dx, y + dy
      if valid:
        edges[v0][v1] = (dx, dy)

  states = set(itertools.product(vertices, CARDINALS))

  steps = collections.defaultdict(lambda: {})
  for s0 in states:
    v0, d0 = s0
    for s1 in states:
      v1, d1 = s1
      if s0 == s1:
        continue
      if v0 == v1:
        if CARDINALS.index(d1) == ((CARDINALS.index(d0) + 1) % len(CARDINALS)):
          steps[s0][s1] = "L"
        elif CARDINALS.index(d0) == ((CARDINALS.index(d1) + 1) % len(CARDINALS)):
          steps[s0][s1] = "R"
      elif d0 == d1 and v1 in edges[v0] and edges[v0][v1] == d0:
        steps[s0][s1] = str(abs(v1[0] - v0[0]) + abs(v1[1] - v0[1]))

  commands = []
  visited = set()
  s0 = start
  while True:
    v0, d0 = s0
    visited.add(v0)
    v1, d1 = None, None
    for v1, d1 in edges[v0].items():
      if v1 in visited:
        continue
      if v1 in ends or v1 in corners:
        break
    s1 = (v1, d1)
    turns = (CARDINALS.index(d1) - CARDINALS.index(d0) + len(CARDINALS)) % len(CARDINALS)
    if turns <= 2:
      commands.extend(("L",) * turns)
    else:
      commands.extend(("R",) * (4 - turns))
    commands.append(steps[(v0, d1)][s1])
    if v1 in ends:
      break
    s0 = s1
  commands = tuple(commands)

  functions = set()
  for length in range(1, 11):
    for start in range(len(commands) - 2 * length + 1):
      function = commands[start:start + length]
      if len(",".join(function)) > 20:
        continue
      functions.add(function)

  best_function_set = None
  best_main = None
  for function_set in itertools.combinations(functions, 3):
    remaining = commands
    main = []
    index = 0
    failed = False
    while remaining:
      best_function = None
      best_function_index = None
      for function_index, function in enumerate(function_set):
        if len(function) <= len(remaining):
          if remaining[:len(function)] == function:
            if best_function is None or len(function) > len(best_function):
              best_function = function
              best_function_index = function_index
      if best_function is None:
        failed = True
        break
      else:
        main.append("ABC"[best_function_index])
        remaining = remaining[len(best_function):]
    if not failed and len(",".join(main)) <= 20:
      best_function_set = function_set
      best_main = main
      break

  program = intcode.IntCode(dict(enumerate(map(int, lines[0].split(",")))))
  program.prog_[0] = 2

  inputs = tuple(",".join(command_list) for command_list in (best_main,) + best_function_set) + ("n",)
  for string in inputs:
    for character in string:
      program.input(ord(character))
    program.input(ord("\n"))

  line = ""
  last_line = []
  while not program.is_done():
    while program.has_output():
      output_int = program.output()
      character = chr(output_int)
      if character == "\n":
        # print(line)
        line = ""
        last_line = []
      else:
        line += character
        last_line.append(output_int)
    program.step()
  yield last_line[0]

if __name__ == "__main__":
  p.main()
