#!/usr/bin/env python

import sys


if __name__ == "__main__":
  all_lines = []
  for wire in sys.stdin:
    wire_lines = []
    start = [0, 0]
    prior_length = 0
    for run in wire.strip().split(","):
      dir = run[0]
      length = int(run[1:])
      end = list(start)
      if dir == "D":
        end[1] -= length
      elif dir == "L":
        end[0] -= length
      elif dir == "R":
        end[0] += length
      elif dir == "U":
        end[1] += length
      wire_lines.append(
        (
          (
            min(start[0], end[0]),
            min(start[1], end[1]),
          ),
          (
            max(start[0], end[0]),
            max(start[1], end[1]),
          ),
          start,
          prior_length
        )
      )
      start = end
      prior_length += length
    all_lines.append(wire_lines)

  closest = 0
  earliest = 0

  for l1_min, l1_max, l1_start, l1_prior_length in all_lines[0]:
    for l2_min, l2_max, l2_start, l2_prior_length in all_lines[1]:
      if (l1_max[0] < l2_min[0] or
          l1_max[1] < l2_min[1] or
          l1_min[0] > l2_max[0] or
          l1_min[1] > l2_max[1]):
        continue
      intersect = [0, 0]
      if l1_min[0] == l1_max[0]:
        intersect[0] = l1_min[0]
      if l1_min[1] == l1_max[1]:
        intersect[1] = l1_min[1]
      if l2_min[0] == l2_max[0]:
        intersect[0] = l2_min[0]
      if l2_min[1] == l2_max[1]:
        intersect[1] = l2_min[1]

      dist = sum(abs(v) for v in intersect)
      if dist > 0 and closest == 0 or dist < closest:
        closest = dist

      l1_length = (l1_prior_length +
                   abs(intersect[0] - l1_start[0]) +
                   abs(intersect[1] - l1_start[1]))
      l2_length = (l2_prior_length +
                   abs(intersect[0] - l2_start[0]) +
                   abs(intersect[1] - l2_start[1]))
      total_length = l1_length + l2_length
      if total_length > 0 and earliest == 0 or total_length < earliest:
        earliest = total_length

  print("closest", closest)
  print("earliest", earliest)
