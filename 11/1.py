#!/usr/bin/env python3

import itertools
import sys

def position_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(parameter, 0)
  else:
    prog[parameter] = write

def immediate_mode(prog, parameter, write=None):
  if write is None:
    return parameter

def relative_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(prog.get("relative_base", 0) + parameter, 0)
  else:
    prog[prog.get("relative_base", 0) + parameter] = write

MODES = [position_mode, immediate_mode, relative_mode]

def use_mode(mode_index, prog, parameter, write=None):
  return MODES[mode_index](prog, parameter, write=write)

class IntCode:

  def __init__(self, prog, input=[]):
    self.index_ = 0
    self.input_ = list(input)
    self.is_done_ = False
    self.is_input_blocked_ = False
    self.output_ = []
    self.prog_ = dict(prog)

  def input(self, value):
    self.input_.append(value)

  def has_output(self):
    return len(self.output_) > 0

  def output(self):
    if self.output_:
      return self.output_.pop(0)
    return None

  def is_input_blocked(self):
    return self.is_input_blocked_

  def is_done(self):
    return self.is_done_

  def run(self):
    while self.step():
      pass

  def step(self):
    if self.is_done_:
      return False
    instr = "{:05}".format(self.prog_[self.index_])
    opcode = int(instr[-2:])
    modes = tuple(int(mode) for mode in instr[:3][::-1])
    params = tuple(self.prog_.get(self.index_ + param + 1, 0) for param in range(3))
    if opcode == 1:
      # Add
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) +
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 2:
      # Multiply
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) *
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 3:
      # Input
      if self.input_:
        self.is_input_blocked = False
      else:
        self.is_input_blocked = True
        return False
      value = self.input_.pop(0)
      use_mode(modes[0], self.prog_, params[0], write=value)
      self.index_ += 2
    elif opcode == 4:
      # Output
      value = use_mode(modes[0], self.prog_, params[0])
      self.output_.append(value)
      self.index_ += 2
    elif opcode == 5:
      # Jump if true
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ += 3
      else:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
    elif opcode == 6:
      # Jump if false
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
      else:
        self.index_ += 3
    elif opcode == 7:
      # Less than
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) <
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 8:
      # Equals
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) ==
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 9:
      # Adjust relative base
      self.prog_["relative_base"] = (self.prog_.get("relative_base", 0) +
                                     use_mode(modes[0], self.prog_, params[0]))
      self.index_ += 2
    elif opcode == 99:
      # Exit
      self.is_done_ = True
      return False
    return True

if __name__ == "__main__":
  prog_string = "".join(line.strip() for line in sys.stdin)
  prog = {key: value for key, value in zip(itertools.count(), (int(val) for val in prog_string.split(",")))}

  canvas = {}

  position = (0, 0)
  direction = (0, -1)

  canvas[position] = 1

  painter = IntCode(prog)

  while not painter.is_done():
    painter.run()

    while painter.has_output():
      colour = painter.output()
      canvas[position] = colour

      turn = painter.output()
      if turn == 0:
        # Left
        direction = (direction[1], -direction[0])
      else:
        # Right
        direction = (-direction[1], direction[0])
      position = (position[0] + direction[0], position[1] + direction[1])

    painter.input(canvas.get(position, 0))

  print("painted panels", len(canvas))

  min_x = 0
  min_y = 0
  max_x = 0
  max_y = 0
  for x, y in canvas.keys():
    if x < min_x:
      min_x = x
    if y < min_y:
      min_y = y
    if x > max_x:
      max_x = x
    if y > max_y:
      max_y = y

  for y in range(min_y, max_y + 1):
    line = ""
    for x in range(min_x, max_x + 1):
      if canvas.get((x, y), 0) == 0:
        line += "."
      else:
        line += "#"
    print(line)
