#!/usr/bin/env python3

import heapq
import itertools
import sys
import time

def position_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(parameter, 0)
  else:
    prog[parameter] = write

def immediate_mode(prog, parameter, write=None):
  if write is None:
    return parameter

def relative_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(prog.get("relative_base", 0) + parameter, 0)
  else:
    prog[prog.get("relative_base", 0) + parameter] = write

MODES = [position_mode, immediate_mode, relative_mode]

def use_mode(mode_index, prog, parameter, write=None):
  return MODES[mode_index](prog, parameter, write=write)

class IntCode:

  def __init__(self, prog, input=[]):
    self.index_ = 0
    self.input_ = list(input)
    self.is_done_ = False
    self.is_input_blocked_ = False
    self.output_ = []
    self.prog_ = dict(prog)

  def input(self, value):
    self.input_.append(value)

  def has_output(self):
    return len(self.output_) > 0

  def output(self):
    if self.output_:
      return self.output_.pop(0)
    return None

  def is_input_blocked(self):
    return self.is_input_blocked_

  def is_done(self):
    return self.is_done_

  def run(self):
    while self.step():
      pass

  def step(self):
    if self.is_done_:
      return False
    instr = "{:05}".format(self.prog_[self.index_])
    opcode = int(instr[-2:])
    modes = tuple(int(mode) for mode in instr[:3][::-1])
    params = tuple(self.prog_.get(self.index_ + param + 1, 0) for param in range(3))
    if opcode == 1:
      # Add
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) +
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 2:
      # Multiply
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) *
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 3:
      # Input
      if self.input_:
        self.is_input_blocked_ = False
      else:
        self.is_input_blocked_ = True
        return False
      value = self.input_.pop(0)
      use_mode(modes[0], self.prog_, params[0], write=value)
      self.index_ += 2
    elif opcode == 4:
      # Output
      value = use_mode(modes[0], self.prog_, params[0])
      self.output_.append(value)
      self.index_ += 2
    elif opcode == 5:
      # Jump if true
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ += 3
      else:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
    elif opcode == 6:
      # Jump if false
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
      else:
        self.index_ += 3
    elif opcode == 7:
      # Less than
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) <
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 8:
      # Equals
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) ==
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 9:
      # Adjust relative base
      self.prog_["relative_base"] = (self.prog_.get("relative_base", 0) +
                                     use_mode(modes[0], self.prog_, params[0]))
      self.index_ += 2
    elif opcode == 99:
      # Exit
      self.is_done_ = True
      return False
    return True

DIRECTIONS = {
  1: (0, -1),
  2: (0, 1),
  3: (-1, 0),
  4: (1, 0),
}

def l1_norm(start, end):
  return abs(end[0] - start[0]) + abs(end[1] - start[1])

def draw_map(map):
  min_x = 0
  min_y = 0
  max_x = 0
  max_y = 0
  for x, y in itertools.chain(map["floors"], map["walls"], map["frontiers"],
                              (map["droid"], map["oxygen_system"] or (0, 0))):
    if x < min_x:
      min_x = x
    if y < min_y:
      min_y = y
    if x > max_x:
      max_x = x
    if y > max_y:
      max_y = y
  print("=" * (max_x - min_x + 1))
  for y in range(min_y, max_y + 1):
    line = ""
    for x in range(min_x, max_x + 1):
      if (x, y) == map["oxygen_system"]:
        line += "O"
      elif (x, y) == map["droid"]:
        line += "D"
      elif (x, y) == (0, 0):
        line += "+"
      elif (x, y) in map["walls"]:
        line += "#"
      elif (x, y) in map["floors"]:
        line += "."
      elif (x, y) in map["frontiers"]:
        line += "?"
      else:
        line += " "
    print(line)
  print("=" * (max_x - min_x + 1))

# Returns a list of (node, direction) values where node is each point on the
# path and direction is the direction to travel to get to the next node
def search_map(map, start, goal):
  # Use A* with a priority queue containing:
  #   (
  #     steps to reach from start + L1 norm to goal,
  #     node,
  #     steps to reach from start,
  #     previous node,
  #     direction travelled from previous node,
  #   )
  opened_nodes = []
  heapq.heappush(opened_nodes, (l1_norm(start, goal), start, 0, None, None))

  # Keep track, for each node:
  #   (
  #     previous node,
  #     direction travelled from previous node,
  #   )
  searched_nodes = {}

  while opened_nodes:
    _, node, steps, previous, direction = heapq.heappop(opened_nodes)
    if node in searched_nodes:
      continue
    searched_nodes[node] = (previous, direction)
    if node == goal:
      break
    for next_direction, (dx, dy) in DIRECTIONS.items():
      next = (node[0] + dx, node[1] + dy)
      if next not in map["walls"] and next not in searched_nodes:
        heapq.heappush(opened_nodes, (steps + 1 + l1_norm(next, goal), next,
                                      steps + 1, node, next_direction))

  if goal not in searched_nodes:
    return None

  path = [(goal, None)]
  step_position = goal
  while step_position != start:
    step_position, direction = searched_nodes[step_position]
    path.append((step_position, direction))
  return path[::-1]

def do_commands(droid, map, commands):
  for command in commands:
    droid.input(command)
    droid.run()
    status = droid.output()

    next_position = (map["droid"][0] + DIRECTIONS[command][0],
                     map["droid"][1] + DIRECTIONS[command][1])

    # Remove explored cell from frontiers set
    if next_position in map["frontiers"]:
      map["frontiers"].remove(next_position)

    # Add oxygen system
    if status == 2:
      map["oxygen_system"] = next_position

    if status in (1, 2):
      # Add floor
      map["floors"].add(next_position)

      # Move to the cell
      map["droid"] = next_position

      # Add any new frontiers
      for dx, dy in DIRECTIONS.values():
        frontier = (map["droid"][0] + dx, map["droid"][1] + dy)
        if (frontier not in map["floors"] and
            frontier not in map["frontiers"] and
            frontier not in map["walls"]):
          map["frontiers"].add(frontier)

    if status == 0:
      # Add wall
      map["walls"].add(next_position)

      # Stop if hit a wall
      break

def main():
  prog_string = "".join(line.strip() for line in sys.stdin)
  prog = {key: value for key, value in zip(itertools.count(), (int(val) for val in prog_string.split(",")))}

  map = {
    "droid": (0, 0),
    "floors": set(),
    "frontiers": set(),
    "oxygen_system": None,
    "walls": set(),
  }

  map["floors"].add(map["droid"])
  for direction in DIRECTIONS.values():
    map["frontiers"].add(direction)

  droid = IntCode(prog)

  # Explore until the oxygen system is found
  while map["frontiers"] and map["oxygen_system"] is None:
    # Get the frontier closest to the droid
    closest_frontier = None
    closest_dist = None
    for frontier in map["frontiers"]:
      dist = l1_norm(map["droid"], frontier)
      if closest_frontier is None or dist < closest_dist:
        closest_frontier = frontier
        closest_dist = dist

    # Go to it
    path = search_map(map, map["droid"], closest_frontier)
    do_commands(droid, map, (direction for _, direction in path[:-1]))

  # Now refine the best path from (0, 0) to oxygen system by finding the best
  # path avoiding just walls then moving to the nearest position on that path
  # that is not a known floor
  while True:
    # Get the best path from (0, 0) to the oxygen system using A* and avoiding walls
    path = search_map(map, (0, 0), map["oxygen_system"])

    # Get the closest unexplored cell in the path
    closest_unknown = None
    closest_dist = None
    for node, _ in path:
      if node not in map["floors"] and node not in map["walls"]:
        dist = l1_norm(map["droid"], node)
        if closest_unknown is None or dist < closest_dist:
          closest_unknown = node
          closest_dist = dist

    # If there are no unknown nodes in the path, then it is optimal
    if closest_unknown is None:
      print("commands", len(path) - 1)
      break

    # Otherwise go to the unknown node
    path = search_map(map, map["droid"], closest_unknown)
    do_commands(droid, map, (direction for _, direction in path[:-1]))

  # Finish exploring the area
  while map["frontiers"]:
    # Get the frontier closest to the droid
    closest_frontier = None
    closest_dist = None
    for frontier in map["frontiers"]:
      dist = l1_norm(map["droid"], frontier)
      if closest_frontier is None or dist < closest_dist:
        closest_frontier = frontier
        closest_dist = dist

    # Go to it
    path = search_map(map, map["droid"], closest_frontier)
    do_commands(droid, map, (direction for _, direction in path[:-1]))

  # Draw the complete map
  draw_map(map)

  # Find the floor that is furthest from the oxygen system
  opened_floors = []
  heapq.heappush(opened_floors, (0, map["oxygen_system"]))
  floor_fill_times = {}
  while opened_floors:
    floor_fill_time, floor = heapq.heappop(opened_floors)
    if floor in floor_fill_times:
      continue
    floor_fill_times[floor] = floor_fill_time
    for dx, dy in DIRECTIONS.values():
      next_floor = (floor[0] + dx, floor[1] + dy)
      if (next_floor in map["floors"] and
          next_floor not in floor_fill_times):
        heapq.heappush(opened_floors, (floor_fill_time + 1, next_floor))

  # That is the amount of time required to fill the whole area
  print("fill time", max(floor_fill_times.values()))

if __name__ == "__main__":
  main()
