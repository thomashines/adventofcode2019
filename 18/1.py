#!/usr/bin/env python3

import heapq
import itertools
import sys
import time

def draw_map(map):
  characters = {}
  for floor in map["floors"]:
    characters[floor] = "."
  for door, position in map["doors"].items():
    characters[position] = door
  for key, position in map["keys"].items():
    characters[position] = key.lower()
  if "dorf" in map:
    characters[map["dorf"]] = "@"
  if "dorfs" in map:
    for dorf in map["dorfs"]:
      characters[dorf] = "@"
  min_x = 1
  min_y = 1
  max_x = 1
  max_y = 1
  for x, y in characters:
    if x < min_x:
      min_x = x
    if y < min_y:
      min_y = y
    if x > max_x:
      max_x = x
    if y > max_y:
      max_y = y
  print("=" * (max_x - min_x + 1))
  for y in range(min_y, max_y + 1):
    line = ""
    for x in range(min_x, max_x + 1):
      line += characters.get((x, y), "#")
    print(line)
  print("=" * (max_x - min_x + 1))

def l1_norm(start, goal):
  return abs(goal[0] - start[0]) + abs(goal[1] - start[1])

SEARCH_CACHE = {}
def search(map, start, goal, keys):
  # print("start", start)
  # print("goal", goal)
  search_cache_key = (tuple(sorted((start, goal))), tuple(sorted(keys)))
  if search_cache_key in SEARCH_CACHE:
    return SEARCH_CACHE[search_cache_key]

  # Each node is:
  # (
  #   steps to reach + l1_norm to goal,
  #   position,
  #   steps to reach,
  #   keys used
  # )
  visited = set()
  nodes = []
  heapq.heappush(nodes, (l1_norm(start, goal), start, 0, ()))
  while nodes:
    _, position, steps, keys_used = heapq.heappop(nodes)
    # print("visit", position, steps, keys_used)
    if position in visited:
      # print("revisit")
      continue
    if position == goal:
      # print("done")
      SEARCH_CACHE[search_cache_key] = (steps, keys_used)
      return (steps, keys_used)
    visited.add(position)
    for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
      next_position = (position[0] + dx, position[1] + dy)
      # print("next_position", next_position)
      if next_position in visited:
        # print("already visited")
        continue
      if next_position not in map["floors"]:
        # print("not floor")
        continue
      key_required = None
      next_keys_used = keys_used
      for door, door_position in map["doors"].items():
        if next_position == door_position:
          key_required = door
          break
      if key_required is not None:
        if key_required not in keys:
          # print("key not available")
          continue
        next_keys_used = tuple(sorted(next_keys_used + (key_required,)))
      heapq.heappush(nodes, (steps + 1 + l1_norm(next_position, goal),
                             next_position, steps + 1, next_keys_used))
  SEARCH_CACHE[search_cache_key] = (None, None)
  return (None, None)

# def

def main():
  map = {
    "dorf": None,
    "floors": set(),
    "doors": {},
    "keys": {},
  }
  y = 0
  for line in sys.stdin:
    x = 0
    for character in line.strip():
      is_dorf = character == "@"
      is_floor = character == "."
      is_door = (ord(character) >= ord("A")) and (ord(character) <= ord("Z"))
      is_key = (ord(character) >= ord("a")) and (ord(character) <= ord("z"))
      if is_dorf or is_floor or is_door or is_key:
        map["floors"].add((x, y))
      if is_dorf:
        map["dorf"] = (x, y)
      if is_door:
        map["doors"][character] = (x, y)
      if is_key:
        map["keys"][character.upper()] = (x, y)
      x += 1
    y += 1

  draw_map(map)

  # Between any 2 locations (i,e, start or keys) all optimal routes require
  # passing through the same set of doors and there are no other routes if any
  # of the doors are locked. Therefore just find all of these routes and do a
  # graph search on them.

  # Build a map of:
  # {
  #   start position:
  #   {
  #     goal position:
  #     (
  #       steps,
  #       keys required,
  #     )
  #   }
  # }
  edges = {}
  all_keys = set(map["keys"].keys())
  locations = set(map["keys"].values())
  locations.add(map["dorf"])
  for start, goal in itertools.combinations(locations, 2):
    steps, keys_required = search(map, start, goal, all_keys)
    if steps is not None:
      for node in (start, goal):
        if node not in edges:
          edges[node] = {}
      edges[start][goal] = (steps, keys_required)
      edges[goal][start] = (steps, keys_required)

  # Build a map of:
  # {
  #   position:
  #   keys acquired
  # }
  key_positions = {}
  for key, position in map["keys"].items():
    key_positions[position] = key

  # Do Dijkstra's search with nodes being:
  # (
  #   steps,
  #   position,
  #   keyset as sorted tuple
  # )
  goal_keyset = tuple(sorted(map["keys"].keys()))
  visited = set()
  nodes = []
  heapq.heappush(nodes, (0, map["dorf"], ()))
  while nodes:
    steps, position, keyset = heapq.heappop(nodes)
    if (position, keyset) in visited:
      continue
    if keyset == goal_keyset:
      print("steps", steps)
      break
    visited.add((position, keyset))
    for next_position, (new_steps, keys_required) in edges[position].items():
      traversable = True
      for key in keys_required:
        if key not in keyset:
          traversable = False
          break
      if traversable:
        next_keyset = tuple(keyset)
        if next_position in key_positions:
          next_keyset = tuple(sorted(set(keyset + (key_positions[next_position],))))
        if (next_position, next_keyset) not in visited:
          heapq.heappush(nodes, (steps + new_steps, next_position, next_keyset))

  map["floors"].remove(map["dorf"])
  map["floors"].remove((map["dorf"][0] - 1, map["dorf"][1]))
  map["floors"].remove((map["dorf"][0] + 1, map["dorf"][1]))
  map["floors"].remove((map["dorf"][0], map["dorf"][1] - 1))
  map["floors"].remove((map["dorf"][0], map["dorf"][1] + 1))

  map["dorfs"] = set((
    (map["dorf"][0] - 1, map["dorf"][1] - 1),
    (map["dorf"][0] - 1, map["dorf"][1] + 1),
    (map["dorf"][0] + 1, map["dorf"][1] - 1),
    (map["dorf"][0] + 1, map["dorf"][1] + 1),
  ))
  del map["dorf"]

  draw_map(map)

  # Build a map of:
  # {
  #   start position:
  #   {
  #     goal position:
  #     (
  #       steps,
  #       keys required,
  #     )
  #   }
  # }
  edges = {}
  all_keys = set(map["keys"].keys())
  locations = set(map["keys"].values())
  locations.update(map["dorfs"])
  for start, goal in itertools.combinations(locations, 2):
    steps, keys_required = search(map, start, goal, all_keys)
    if steps is not None:
      for node in (start, goal):
        if node not in edges:
          edges[node] = {}
      edges[start][goal] = (steps, keys_required)
      edges[goal][start] = (steps, keys_required)

      # Check if there are is an alternative route
      for exclude_key in keys_required:
        alternative_steps, alternative_keys_required = search(map, start, goal, all_keys - set((exclude_key,)))
        if alternative_steps is not None:
          print("alternative route")

  # Do Dijkstra's search with nodes being:
  # (
  #   steps,
  #   positions,
  #   keyset as sorted tuple
  # )
  goal_keyset = tuple(sorted(map["keys"].keys()))
  visited = set()
  nodes = []
  heapq.heappush(nodes, (0, tuple(sorted(map["dorfs"])), ()))
  while nodes:
    steps, positions, keyset = heapq.heappop(nodes)
    if (positions, keyset) in visited:
      continue
    if keyset == goal_keyset:
      print("steps", steps)
      break
    visited.add((positions, keyset))
    for dorf in range(len(positions)):
      position = positions[dorf]
      other_positions = positions[:dorf] + positions[dorf + 1:]
      for next_position, (new_steps, keys_required) in edges[position].items():
        traversable = True
        for key in keys_required:
          if key not in keyset:
            traversable = False
            break
        if traversable:
          next_keyset = tuple(keyset)
          if next_position in key_positions:
            next_keyset = tuple(sorted(set(keyset + (key_positions[next_position],))))
          next_positions = tuple(sorted(other_positions + (next_position,)))
          if (next_positions, next_keyset) not in visited:
            heapq.heappush(nodes, (steps + new_steps, next_positions, next_keyset))

if __name__ == "__main__":
  main()
