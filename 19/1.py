#!/usr/bin/env python3

import heapq
import itertools
import sys
import time

def position_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(parameter, 0)
  else:
    prog[parameter] = write

def immediate_mode(prog, parameter, write=None):
  if write is None:
    return parameter

def relative_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(prog.get("relative_base", 0) + parameter, 0)
  else:
    prog[prog.get("relative_base", 0) + parameter] = write

MODES = [position_mode, immediate_mode, relative_mode]

def use_mode(mode_index, prog, parameter, write=None):
  return MODES[mode_index](prog, parameter, write=write)

class IntCode:

  def __init__(self, prog, input=[]):
    self.index_ = 0
    self.input_ = list(input)
    self.is_done_ = False
    self.is_input_blocked_ = False
    self.output_ = []
    self.prog_ = dict(prog)

  def input(self, value):
    self.input_.append(value)

  def has_output(self):
    return len(self.output_) > 0

  def output(self):
    if self.output_:
      return self.output_.pop(0)
    return None

  def is_input_blocked(self):
    return self.is_input_blocked_

  def is_done(self):
    return self.is_done_

  def run(self):
    while self.step():
      pass

  def step(self):
    if self.is_done_:
      return False
    instr = "{:05}".format(self.prog_[self.index_])
    opcode = int(instr[-2:])
    modes = tuple(int(mode) for mode in instr[:3][::-1])
    params = tuple(self.prog_.get(self.index_ + param + 1, 0) for param in range(3))
    if opcode == 1:
      # Add
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) +
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 2:
      # Multiply
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) *
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 3:
      # Input
      if self.input_:
        self.is_input_blocked_ = False
      else:
        self.is_input_blocked_ = True
        return False
      value = self.input_.pop(0)
      use_mode(modes[0], self.prog_, params[0], write=value)
      self.index_ += 2
    elif opcode == 4:
      # Output
      value = use_mode(modes[0], self.prog_, params[0])
      self.output_.append(value)
      self.index_ += 2
    elif opcode == 5:
      # Jump if true
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ += 3
      else:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
    elif opcode == 6:
      # Jump if false
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
      else:
        self.index_ += 3
    elif opcode == 7:
      # Less than
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) <
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 8:
      # Equals
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) ==
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 9:
      # Adjust relative base
      self.prog_["relative_base"] = (self.prog_.get("relative_base", 0) +
                                     use_mode(modes[0], self.prog_, params[0]))
      self.index_ += 2
    elif opcode == 99:
      # Exit
      self.is_done_ = True
      return False
    return True

def get_position(in_beam, y_extents, prog, position):
  if position in in_beam:
    return in_beam[position]
  if position[0] in y_extents:
    known, (min_y, max_y) = y_extents[position[0]]
    if min_y <= position[1] <= max_y:
      return True
    elif known:
      return False
  drone = IntCode(prog)
  drone.input(position[0])
  drone.input(position[1])
  drone.run()
  in_beam[position] = (drone.output() == 1)
  if in_beam[position]:
    if position[0] in y_extents and not y_extents[position[0]][0]:
      known, (min_y, max_y) = y_extents[position[0]]
      y_extents[position[0]] = (known, (min(min_y, position[0]), max(max_y, position[0])))
    else:
      y_extents[position[0]] = (False, (position[0], position[0]))
  if position[0] in y_extents and not y_extents[position[0]][0]:
    _, (min_y, max_y) = y_extents[position[0]]
    while in_beam.get((position[0], min_y - 1), False):
      min_y -= 1
    while in_beam.get((position[0], max_y + 1), False):
      min_y += 1
    known = (in_beam.get((position[0], min_y - 1)) == False) and (in_beam.get((position[0], max_y + 1)) == False)
    y_extents[position[0]] = (known, (min_y, max_y))
  return in_beam[position]

COUNT_IN_BEAMS_IN_SQUARE_CACHE = {}
def count_in_beams_in_square(in_beam, y_extents, prog, top_left, size=100):
  global COUNT_IN_BEAMS_IN_SQUARE_CACHE
  if (top_left, size) in COUNT_IN_BEAMS_IN_SQUARE_CACHE:
    return COUNT_IN_BEAMS_IN_SQUARE_CACHE[top_left, size]
  in_beams = 0
  for x in range(top_left[0], top_left[0] + size):
    for y in range(top_left[1], top_left[1] + size):
      if get_position(in_beam, y_extents, prog, (x, y)):
        in_beams += 1
  COUNT_IN_BEAMS_IN_SQUARE_CACHE[top_left, size] = in_beams
  return in_beams

def main():
  prog_string = "".join(line.strip() for line in sys.stdin)
  prog = {key: value for key, value in zip(itertools.count(), (int(val) for val in prog_string.split(",")))}

  in_beam = {}
  y_extents = {}

  start = (0, 0)

  visited = set()

  best_position = (0, 0)
  best_in_beams = 0

  open = []
  heapq.heappush(open, (-count_in_beams_in_square(in_beam, y_extents, prog, start), sum(start), start))
  while open:
    in_beams, dist, position = heapq.heappop(open)
    in_beams = -in_beams
    if (in_beams, position) in visited:
      continue
    if (in_beams > best_in_beams) or ((in_beams == best_in_beams) and (sum(position) < sum(best_position))):
      best_position = position
      best_in_beams = in_beams
      print("best_position", best_position, best_in_beams, 10000 * best_position[0] + best_position[1])
    visited.add((in_beams, position))
    for position_change in itertools.product((-1, 0, 1), repeat=2):
      next_position = (position[0] + position_change[0], position[1] + position_change[1])
      if next_position[0] < 0 or next_position[1] < 0:
        continue
      dist = sum(next_position)
      in_beams = count_in_beams_in_square(in_beam, y_extents, prog, next_position)
      if (in_beams, next_position) in visited:
        continue
      heapq.heappush(open, (-in_beams, dist, next_position))

if __name__ == "__main__":
  main()
