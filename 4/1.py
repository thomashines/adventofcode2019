#!/usr/bin/env python

import sys

def increment(password, index=0):
  # print("password", password)
  if index == len(password):
    pass
  elif index == len(password) - 1:
    password[index] = (password[index] + 1) % 10
  else:
    increment(password, index=index + 1)
    if password[index + 1] == 0:
      password[index] = (password[index] + 1) % 10
      for after_index in range(index, len(password)):
        if password[after_index] < password[index]:
          password[after_index] = password[index]

if __name__ == "__main__":
  stdin = "".join(line.strip() for line in sys.stdin)
  pw_min, pw_max = list(list(int(digit) for digit in val) for val in stdin.split("-"))

  password = list(pw_min)
  for index in range(len(password) - 1):
    if password[index] > password[index + 1]:
      password[index + 1] = password[index]

  passwords_1 = 0
  passwords_2 = 0
  while password <= pw_max:

    for index in range(len(password) - 1):
      if password[index] > password[index + 1]:
        print("bad password")
        sys.exit(1)
      if password[index] == password[index + 1]:
        passwords_1 += 1
        break

    is_password_2 = False
    count = 1
    counts = set()
    count_char = password[0]
    for index in range(1, len(password)):
      if password[index] == count_char:
        count += 1
      else:
        if count == 2:
          is_password_2 = True
          break
        count_char = password[index]
        count = 1
    if is_password_2 or count == 2:
      passwords_2 += 1

    increment(password)

  print("passwords_1", passwords_1)
  print("passwords_2", passwords_2)
