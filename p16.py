#!/usr/bin/env python3

import numpy

import p

def flatten(signal):
  value = 0
  for index, digit in enumerate(reversed(signal)):
    value += digit * (10 ** index)
  return value

BASE = (0, 1, 0, -1)
def pattern_coefficient(row, column):
  return BASE[((row + 1) // (column + 1)) % len(BASE)]

def fft(signal):
  size = len(signal)
  signal = numpy.array(signal)
  patterns = numpy.fromfunction(numpy.vectorize(pattern_coefficient), (size, size), dtype=int)
  vector_mod10 = numpy.vectorize(lambda number: abs(number) % 10)
  for phase in range(100):
    signal = vector_mod10(numpy.dot(signal, patterns))
  return list(signal)

def fft_tail(signal, offset):
  size = len(signal)
  size_tail = size - offset
  this_signal = list(reversed(signal[offset:]))
  for iteration in range(100):
    next_signal = [0] * size_tail
    total = 0
    for index in range(size_tail):
      total += this_signal[index]
      next_signal[index] = total % 10
    this_signal, next_signal = next_signal, this_signal
  return tuple(reversed(this_signal))

@p.puzzle(16, 1)
def main(lines):
  signal = tuple(map(int, lines[0]))
  yield flatten(fft(signal)[:8])
  signal = signal * 10000
  offset = flatten(signal[:7])
  if (len(signal) // 2 < offset) and ((offset + 8) < len(signal)):
    yield flatten(fft_tail(signal, offset)[:8])

if __name__ == "__main__":
  p.main()
