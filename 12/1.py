#!/usr/bin/env python3

import functools
import itertools
import operator
import re
import sys

def greatest_common_divisor(values):
  if len(values) == 0:
    return None
  elif len(values) == 1:
    return values[0]
  elif len(values) == 2:
    if values == (0, 0):
      return None
    elif values[0] == 0:
      return values[1]
    elif values[1] == 0:
      return values[0]
    elif values[0] == values[1]:
      return values[0]
    elif values[0] > values[1]:
      return greatest_common_divisor((values[0] % values[1], values[1]))
    else:
      return greatest_common_divisor((values[0], values[1] % values[0]))
  else:
    return greatest_common_divisor((values[0], greatest_common_divisor(values[1:])))

def least_common_multiple(values):
  if len(values) == 0:
    return None
  elif len(values) == 1:
    return values[0]
  elif len(values) == 2:
    return abs(values[0] * values[1]) // greatest_common_divisor(values)
  else:
    return least_common_multiple((values[0], least_common_multiple(values[1:])))

planet_parser = re.compile("^<x=(\-?\d+), y=(\-?\d+), z=(\-?\d+)>$")
def main():
  planets = list()
  for line in sys.stdin:
    parsed = planet_parser.search(line.strip())
    planets.append({
      "position": list(int(val) for val in parsed.groups()),
      "velocity": [0, 0, 0]
    })
  planet_count = len(planets)

  part1_positions = list()
  part1_velocities = list()

  starts = list()
  periods = list()
  for dimension in range(3):
    states_set = set()
    states_list = list()

    positions = list(planet["position"][dimension] for planet in planets)
    velocities = list(planet["velocity"][dimension] for planet in planets)

    for step in itertools.count(1):

      state = (tuple(positions), tuple(velocities))

      if state in states_set:
        start = states_list.index(state)
        period = len(states_list) - start
        starts.append(start)
        periods.append(period)
        break

      states_set.add(state)
      states_list.append(state)

      for planet in range(planet_count):
        position = positions[planet]
        for other_position in positions:
          if other_position > position:
            velocities[planet] += 1
          elif other_position < position:
            velocities[planet] -= 1

      for planet in range(planet_count):
        positions[planet] = positions[planet] + velocities[planet]

      if step == 1000:
        part1_positions.append(list(positions))
        part1_velocities.append(list(velocities))

  energy = 0
  for planet in range(planet_count):
    potential = 0
    kinetic = 0
    for dimension in range(3):
      potential += abs(part1_positions[dimension][planet])
      kinetic += abs(part1_velocities[dimension][planet])
    energy += potential * kinetic
  print("energy", energy)

  print("common period", least_common_multiple(periods))

if __name__ == "__main__":
  main()
