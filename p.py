#!/usr/bin/env python3

import time

def product(values):
  result = 1
  for value in values:
    result = result * value
  return result

puzzles = []
def puzzle(day, cases):
  def puzzle_imp(func):
    for case in range(cases):
      name = "p{}-{}".format(day, case)
      with open("{}.in".format(name), "r") as input:
        lines = tuple(line.strip("\r\n") for line in input.readlines())
        puzzles.append((name, func, lines))
    return func
  return puzzle_imp

def main():
  def solve_all():
    for name, func, lines in puzzles:
      print("#", name)
      answers = func(lines)
      time_start = time.perf_counter()
      for part, answer in enumerate(answers):
        time_end = time.perf_counter()
        duration = time_end - time_start
        print(" #", "{}-{}".format(name, part), duration * 1e6, "us")
        print("  =", answer)
        time_start = time.perf_counter()
  time_start = time.perf_counter()
  solve_all()
  time_end = time.perf_counter()
  duration = time_end - time_start
  print("# all in", duration * 1e3, "ms")

if __name__ == "__main__":
  main()
