#!/usr/bin/env python3

import heapq
import itertools
import sys
import time

def position_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(parameter, 0)
  else:
    prog[parameter] = write

def immediate_mode(prog, parameter, write=None):
  if write is None:
    return parameter

def relative_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(prog.get("relative_base", 0) + parameter, 0)
  else:
    prog[prog.get("relative_base", 0) + parameter] = write

MODES = [position_mode, immediate_mode, relative_mode]

def use_mode(mode_index, prog, parameter, write=None):
  return MODES[mode_index](prog, parameter, write=write)

class IntCode:

  def __init__(self, prog, input=[]):
    self.index_ = 0
    self.input_ = list(input)
    self.is_done_ = False
    self.is_input_blocked_ = False
    self.output_ = []
    self.prog_ = dict(prog)

  def has_input(self):
    return len(self.input_) > 0

  def input(self, value):
    self.input_.append(value)

  def input_copy(self):
    return list(self.input_)

  def input_size(self):
    return len(self.input_)

  def has_output(self):
    return len(self.output_) > 0

  def output(self):
    if self.output_:
      return self.output_.pop(0)
    return None

  def output_size(self):
    return len(self.output_)

  def is_input_blocked(self):
    return self.is_input_blocked_

  def is_done(self):
    return self.is_done_

  def run(self):
    while self.step():
      pass

  def step(self):
    if self.is_done_:
      return False
    instr = "{:05}".format(self.prog_[self.index_])
    opcode = int(instr[-2:])
    modes = tuple(int(mode) for mode in instr[:3][::-1])
    params = tuple(self.prog_.get(self.index_ + param + 1, 0) for param in range(3))
    if opcode == 1:
      # Add
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) +
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 2:
      # Multiply
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) *
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 3:
      # Input
      if self.input_:
        self.is_input_blocked_ = False
      else:
        self.is_input_blocked_ = True
        return False
      value = self.input_.pop(0)
      use_mode(modes[0], self.prog_, params[0], write=value)
      self.index_ += 2
    elif opcode == 4:
      # Output
      value = use_mode(modes[0], self.prog_, params[0])
      self.output_.append(value)
      self.index_ += 2
    elif opcode == 5:
      # Jump if true
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ += 3
      else:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
    elif opcode == 6:
      # Jump if false
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
      else:
        self.index_ += 3
    elif opcode == 7:
      # Less than
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) <
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 8:
      # Equals
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) ==
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 9:
      # Adjust relative base
      self.prog_["relative_base"] = (self.prog_.get("relative_base", 0) +
                                     use_mode(modes[0], self.prog_, params[0]))
      self.index_ += 2
    elif opcode == 99:
      # Exit
      self.is_done_ = True
      return False
    return True

def main():
  prog_string = "".join(line.strip() for line in sys.stdin)
  prog = {key: value for key, value in zip(itertools.count(), (int(val) for val in prog_string.split(",")))}

  computers = [IntCode(prog, input=[address]) for address in range(50)]

  has_input = [False for computer in computers]

  nat_X = None
  nat_Y = None

  last_nat_X = None
  last_nat_Y = None

  done = False
  while not done:

    for source_address in range(len(computers)):

      # Step forward once
      computers[source_address].step()

      # Give a -1 if input blocked
      if computers[source_address].is_input_blocked():
        has_input[source_address] = False
        computers[source_address].input(-1)
        computers[source_address].step()

      # Check for output
      if computers[source_address].output_size() >= 3:
        destination_address = computers[source_address].output()
        X = computers[source_address].output()
        Y = computers[source_address].output()
        if destination_address == 255:
          nat_X = X
          nat_Y = Y
          print("NAT received", nat_X, nat_Y)
        else:
          computers[destination_address].input(X)
          computers[destination_address].input(Y)
          has_input[destination_address] = True

    # Use NAT if idle
    if not any(has_input) and nat_X is not None and nat_Y is not None:
      computers[0].input(nat_X)
      computers[0].input(nat_Y)
      has_input[0] = True

      if nat_Y == last_nat_Y:
        print("NAT has sent Y", nat_Y, "twice")
        done = True

      last_nat_X = nat_X
      last_nat_Y = nat_Y

if __name__ == "__main__":
  main()
