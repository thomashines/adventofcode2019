#!/usr/bin/env python3

import sys
import pulp

def main():

  problem = pulp.LpProblem("nanofactory", pulp.LpMinimize)

  chemicals = set()
  reactions = []
  for line in sys.stdin:
    reaction = {}
    reaction["name"] = line.strip()
    reaction["count"] = pulp.LpVariable(name="x_[{}]".format(reaction["name"]),
                                        lowBound=0, cat="Integer")

    for key, values in zip(("reagents", "products"), reaction["name"].split(" => ")):
      reaction[key] = {}
      for part in values.split(", "):
        quantity, chemical = part.split(" ")
        reaction[key][chemical] = int(quantity)
        chemicals.add(chemical)

    reactions.append(reaction)

  problem += pulp.lpSum(
    reaction["reagents"].get("ORE", 0) * reaction["count"] for
    reaction in reactions)

  for chemical in chemicals:
    if chemical != "ORE":
      minimum = 1 if chemical == "FUEL" else 0
      problem += pulp.lpSum(
        (reaction["products"].get(chemical, 0) -
         reaction["reagents"].get(chemical, 0)) * reaction["count"] for
        reaction in reactions) >= minimum

  problem.solve()

  print(int(sum(
    reaction["reagents"].get("ORE", 0) * pulp.value(reaction["count"]) for
    reaction in reactions)))

  problem.setObjective(pulp.lpSum(
    -reaction["products"].get("FUEL", 0) * reaction["count"] for
    reaction in reactions))

  problem += pulp.lpSum(
    reaction["reagents"].get("ORE", 0) * reaction["count"] for
    reaction in reactions) <= 1000000000000

  problem.solve()

  print(int(sum(
    reaction["products"].get("FUEL", 0) * pulp.value(reaction["count"]) for
    reaction in reactions)))

if __name__ == "__main__":
  main()
