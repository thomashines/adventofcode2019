#!/usr/bin/env python3

import heapq
import itertools
import sys
import time

def position_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(parameter, 0)
  else:
    prog[parameter] = write

def immediate_mode(prog, parameter, write=None):
  if write is None:
    return parameter

def relative_mode(prog, parameter, write=None):
  if write is None:
    return prog.get(prog.get("relative_base", 0) + parameter, 0)
  else:
    prog[prog.get("relative_base", 0) + parameter] = write

MODES = [position_mode, immediate_mode, relative_mode]

def use_mode(mode_index, prog, parameter, write=None):
  return MODES[mode_index](prog, parameter, write=write)

class IntCode:

  def __init__(self, prog, input=[]):
    self.index_ = 0
    self.input_ = list(input)
    self.is_done_ = False
    self.is_input_blocked_ = False
    self.output_ = []
    self.prog_ = dict(prog)

  def has_input(self):
    return len(self.input_) > 0

  def input(self, value):
    self.input_.append(value)

  def input_copy(self):
    return list(self.input_)

  def input_size(self):
    return len(self.input_)

  def has_output(self):
    return len(self.output_) > 0

  def output(self):
    if self.output_:
      return self.output_.pop(0)
    return None

  def output_size(self):
    return len(self.output_)

  def is_input_blocked(self):
    return self.is_input_blocked_

  def is_done(self):
    return self.is_done_

  def run(self):
    while self.step():
      pass

  def step(self):
    if self.is_done_:
      return False
    instr = "{:05}".format(self.prog_[self.index_])
    opcode = int(instr[-2:])
    modes = tuple(int(mode) for mode in instr[:3][::-1])
    params = tuple(self.prog_.get(self.index_ + param + 1, 0) for param in range(3))
    if opcode == 1:
      # Add
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) +
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 2:
      # Multiply
      use_mode(modes[2], self.prog_, params[2],
               write=(use_mode(modes[0], self.prog_, params[0]) *
                      use_mode(modes[1], self.prog_, params[1])))
      self.index_ += 4
    elif opcode == 3:
      # Input
      if self.input_:
        self.is_input_blocked_ = False
      else:
        self.is_input_blocked_ = True
        return False
      value = self.input_.pop(0)
      use_mode(modes[0], self.prog_, params[0], write=value)
      self.index_ += 2
    elif opcode == 4:
      # Output
      value = use_mode(modes[0], self.prog_, params[0])
      self.output_.append(value)
      self.index_ += 2
    elif opcode == 5:
      # Jump if true
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ += 3
      else:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
    elif opcode == 6:
      # Jump if false
      if use_mode(modes[0], self.prog_, params[0]) == 0:
        self.index_ = use_mode(modes[1], self.prog_, params[1])
      else:
        self.index_ += 3
    elif opcode == 7:
      # Less than
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) <
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 8:
      # Equals
      value = 1 if (use_mode(modes[0], self.prog_, params[0]) ==
                    use_mode(modes[1], self.prog_, params[1])) else 0
      use_mode(modes[2], self.prog_, params[2], write=value)
      self.index_ += 4
    elif opcode == 9:
      # Adjust relative base
      self.prog_["relative_base"] = (self.prog_.get("relative_base", 0) +
                                     use_mode(modes[0], self.prog_, params[0]))
      self.index_ += 2
    elif opcode == 99:
      # Exit
      self.is_done_ = True
      return False
    return True

DOORS = set((
  "east",
  "north",
  "south",
  "west",
))

OPPOSITES = {
  "east": "west",
  "north": "south",
  "south": "north",
  "west": "east",
}

def read_output(droid, map):
  # Get the output
  lines = []
  line = ""
  while droid.has_output():
    character = chr(droid.output())
    if character == "\n":
      if len(line) > 0:
        lines.append(line)
        line = ""
    else:
      line += character
  for line in lines:
    if "Oh, hello!" in line:
      print(line)
      sys.exit(0)

  # Get the location of the last place description in the output
  last_place_line = -1
  for line in range(len(lines)):
    if lines[line].startswith("== "):
      last_place_line = line

  if last_place_line >= 0:

    # Get current location
    map["current_place"] = lines[last_place_line][3:-3]

    # Add to the places list
    if map["current_place"] not in map["places"]:
      map["places"][map["current_place"]] = {
        "doors": {},
        "items": set(),
      }

    # Get door options
    if "Doors here lead:" in lines[last_place_line:]:
      doors_start_line = lines.index("Doors here lead:", last_place_line)
      for line in range(doors_start_line + 1, len(lines) + 1):
        matches = False
        for door in DOORS:
          if lines[line] == ("- " + door):
            if door not in map["places"][map["current_place"]]["doors"]:
              map["places"][map["current_place"]]["doors"][door] = None
              matches = True
        if not matches:
          break

    # Get item options
    map["places"][map["current_place"]]["items"].clear()
    if "Items here:" in lines[last_place_line:]:
      items_start_line = lines.index("Items here:", last_place_line)
      for line in range(items_start_line + 1, len(lines) + 1):
        if lines[line].startswith("- "):
          map["places"][map["current_place"]]["items"].add(lines[line][2:])
        else:
          break

def find_unexplored(map, ignore_places=("Security Checkpoint",)):
  for place, details in map["places"].items():
    if place in ignore_places:
      continue
    for door, next_place in details["doors"].items():
      if next_place is None:
        return place, door
  return None, None

def find_uncollected_item(map, ignore_items=("escape pod",
                                             "giant electromagnet",
                                             "molten lava", "photons",
                                             "infinite loop")):
  for place, details in map["places"].items():
    for item in details["items"]:
      if item not in ignore_items:
        return place, item
  return None, None

def search(map, start, goal):
  doors = {}
  open = [(start, None, None)]
  visited = set()
  while open:
    place, previous_place, previous_door = open.pop(0)
    if place in visited:
      continue
    visited.add(place)
    if previous_place is not None:
      doors[place] = (previous_place, previous_door)
    if place == goal:
      break
    for next_door, next_place in map["places"][place]["doors"].items():
      if next_place is not None and next_place not in visited:
        open.append((next_place, place, next_door))
  path = []
  place = goal
  while place != start:
    previous_place, previous_door = doors[place]
    path.append(previous_door)
    place = previous_place
  return path[::-1]

def traverse_door(droid, map, door):
  start_place = map["current_place"]
  for character in door:
    droid.input(ord(character))
  droid.input(ord("\n"))
  droid.run()
  read_output(droid, map)
  end_place = map["current_place"]
  if start_place != end_place:
    map["places"][start_place]["doors"][door] = end_place
    map["places"][end_place]["doors"][OPPOSITES[door]] = start_place

def take_item(droid, map, item):
  for character in "take {}".format(item):
    droid.input(ord(character))
  droid.input(ord("\n"))
  droid.run()
  read_output(droid, map)
  map["inventory"].add(item)
  map["places"][map["current_place"]]["items"].remove(item)

def drop_item(droid, map, item):
  for character in "drop {}".format(item):
    droid.input(ord(character))
  droid.input(ord("\n"))
  droid.run()
  read_output(droid, map)
  map["inventory"].remove(item)
  map["places"][map["current_place"]]["items"].add(item)

def main():
  prog_string = "".join(line.strip() for line in open(sys.argv[1], "r").read())
  prog = {key: value for key, value in zip(itertools.count(), (int(val) for val in prog_string.split(",")))}

  droid = IntCode(prog)

  map = {
    "places": {},
    "current_place": None,
    "inventory": set(),
  }

  # Initialise the map
  droid.run()
  read_output(droid, map)

  # Explore fully
  while True:
    # Find the next place to explore to
    goal_place, goal_door = find_unexplored(map)
    if goal_place is None:
      break

    # Get a path to the place
    path = search(map, map["current_place"], goal_place)
    path.append(goal_door)

    # Follow the path
    for door in path:
      traverse_door(droid, map, door)

  # Collect all items
  while True:
    # Find the next item to collect
    goal_place, goal_item = find_uncollected_item(map)
    if goal_place is None:
      break

    # Get a path to the place
    path = search(map, map["current_place"], goal_place)

    # Follow the path
    for door in path:
      traverse_door(droid, map, door)

    # Take the item
    take_item(droid, map, goal_item)

  # Go to the Security Checkpoint
  path = search(map, map["current_place"], "Security Checkpoint")
  for door in path:
    traverse_door(droid, map, door)

  # Try every combination of item
  all_items = set(map["inventory"])
  for size in range(1, len(all_items) + 1):
    for inventory in sorted(itertools.combinations(all_items, size)):
      # Drop all items not in inventory
      for item in all_items:
        if item in inventory and item not in map["inventory"]:
          take_item(droid, map, item)
        elif item not in inventory and item in map["inventory"]:
          drop_item(droid, map, item)
      # Try to go through the door
      traverse_door(droid, map, "east")

if __name__ == "__main__":
  main()
