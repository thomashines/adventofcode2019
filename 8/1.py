#!/usr/bin/env python3

import sys

def main():
  pixels = list(int(digit) for digit in "".join(line.strip() for line in sys.stdin))

  width = 25
  height = 6

  layers = []
  layer = []
  row = []

  for pixel in pixels:
    row.append(pixel)
    if len(row) >= width:
      layer.append(row)
      row = []
      if len(layer) >= height:
        layers.append(layer)
        layer = []

  zeros = width * height
  checksum = 0
  for layer in layers:
    layer_zeros = sum(sum(1 if pixel == 0 else 0 for pixel in row) for row in layer)
    if layer_zeros < zeros:
      layer_ones = sum(sum(1 if pixel == 1 else 0 for pixel in row) for row in layer)
      layer_twos = sum(sum(1 if pixel == 2 else 0 for pixel in row) for row in layer)
      checksum = layer_ones * layer_twos
      zeros = layer_zeros
  print("checksum", checksum)

  image = list(list(2 for pixel in range(width)) for row in range(height))
  for layer in layers:
    for row in range(height):
      for pixel in range(width):
        if image[row][pixel] == 2:
          image[row][pixel] = layer[row][pixel]

  for row in image:
    print("".join(" " if pixel == 0 else "#" for pixel in row))

if __name__ == "__main__":
  main()
