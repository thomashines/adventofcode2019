#!/usr/bin/env python

import itertools
import sys

def map_tuple(map, width, height, min_level, max_level):
  return tuple(sorted(map.items()))

def get_adjacent_cells(width, height, level, x, y):
  middle_x, middle_y = width // 2, height // 2
  for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
    adjacent_x, adjacent_y = x + dx, y + dy

    # Go up a level
    if adjacent_x < 0:
      yield (level - 1, middle_x - 1, middle_y)
    elif adjacent_x >= width:
      yield (level - 1, middle_x + 1, middle_y)
    elif adjacent_y < 0:
      yield (level - 1, middle_x, middle_y - 1)
    elif adjacent_y >= height:
      yield (level - 1, middle_x, middle_y + 1)

    # Go down a level
    elif (adjacent_x, adjacent_y) == (middle_x, middle_y):
      if dx == 1:
        for lower_adjacent_y in range(height):
          yield (level + 1, 0, lower_adjacent_y)
      elif dx == -1:
        for lower_adjacent_y in range(height):
          yield (level + 1, width - 1, lower_adjacent_y)
      elif dy == 1:
        for lower_adjacent_x in range(width):
          yield (level + 1, lower_adjacent_x, 0)
      elif dy == -1:
        for lower_adjacent_x in range(width):
          yield (level + 1, lower_adjacent_x, height - 1)

    # Same level
    else:
      yield (level, adjacent_x, adjacent_y)

def print_map(map, width, height, level):
  for y in range(height):
    print("".join(("#" if (map.get((level, x, y), 0) == 1) else ".") for x in range(width)))

def main():
  original_map = {}
  width = 0
  height = 0

  level = 0
  y = 0
  x = 0

  for line in sys.stdin:
    x = 0
    for character in line.strip():
      if character == "#":
        original_map[level, x, y] = 1
      x += 1
    y += 1
  width = x
  height = y
  middle_x = width // 2
  middle_y = height // 2

  maps_list = list()
  maps_set = set()

  map = dict(original_map)
  for step in itertools.count():
    map_tupled = map_tuple(map, width, height, 0, 0)

    if map_tupled in maps_set:
      rating = 0
      index = 0
      for y in range(height):
        for x in range(width):
          rating += map[0, x, y] * (2 ** index)
          index += 1
      print("step", step)
      print("rating", rating)
      break

    maps_list.append(map_tupled)
    maps_set.add(map_tupled)

    next_map = dict(map)
    for y in range(height):
      for x in range(height):
        is_bug = map.get((0, x, y), 0) == 1
        adjacent_bugs = 0
        for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
          adjacent_bugs += map.get((0, x + dx, y + dy), 0)
        if is_bug and adjacent_bugs != 1:
          next_map[0, x, y] = 0
        elif not is_bug and (adjacent_bugs == 1 or adjacent_bugs == 2):
          next_map[0, x, y] = 1
    map = next_map

  min_level = 0
  max_level = 0

  map = dict(original_map)
  for step in itertools.count():
    if step in (10, 200):
      print("step", step)
      bugs = 0
      for level in range(min_level, max_level + 1):
        for y in range(height):
          for x in range(width):
            bugs += map.get((level, x, y), 0)
      print("bugs", bugs)
      if step == 200:
        break

    next_map = dict(map)
    for level in range(min_level - 1, max_level + 2):
      for y in range(height):
        for x in range(height):
          if (x, y) == (middle_x, middle_y):
            continue
          is_bug = map.get((level, x, y), 0) == 1
          adjacent_bugs = 0
          for adjacent_cell in get_adjacent_cells(width, height, level, x, y):
            adjacent_bugs += map.get(adjacent_cell, 0)
          if is_bug and adjacent_bugs != 1:
            next_map[level, x, y] = 0
          elif not is_bug and (adjacent_bugs == 1 or adjacent_bugs == 2):
            next_map[level, x, y] = 1
            min_level = min(min_level, level)
            max_level = max(max_level, level)
    map = next_map

if __name__ == "__main__":
  main()
