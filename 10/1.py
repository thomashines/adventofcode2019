#!/usr/bin/env python3

import itertools
import math
import sys

if __name__ == "__main__":
  asteroids = {}
  y = 0
  for line in sys.stdin:
    x = 0
    for char in line:
      if char == "#":
        asteroids[x, y] = {
          "visible": 0,
          "lines": set(),
        }
      x += 1
    y += 1

  scale = 1000000

  lines = {}
  for asteroid_a in asteroids.keys():
    for asteroid_b in asteroids.keys():
      if asteroid_a != asteroid_b:
        if asteroid_a[0] == asteroid_b[0]:
          slope = float("inf")
          intercept = round(scale * asteroid_a[0])
        else:
          slope = (asteroid_b[1] - asteroid_a[1]) / (asteroid_b[0] - asteroid_a[0])
          intercept = asteroid_a[1] - slope * asteroid_a[0]
          slope = round(scale * slope)
          intercept = round(scale * intercept)
        line = (slope, intercept)
        if line not in lines:
          lines[line] = set()
        lines[line].add(asteroid_a)
        lines[line].add(asteroid_b)
        asteroids[asteroid_a]["lines"].add(line)
        asteroids[asteroid_b]["lines"].add(line)

  for line in lines.keys():
    lines[line] = list(sorted(lines[line]))

  for line, asteroids_in_line in lines.items():
    asteroids[asteroids_in_line[0]]["visible"] += 1
    asteroids[asteroids_in_line[-1]]["visible"] += 1
    for asteroid in asteroids_in_line[1:-1]:
      asteroids[asteroid]["visible"] += 2

  best_visible = 0
  best_asteroid = 0
  for asteroid in asteroids.keys():
    if asteroids[asteroid]["visible"] > best_visible:
      best_visible = asteroids[asteroid]["visible"]
      best_asteroid = asteroid
  print("best_visible", best_visible)
  print("best_asteroid", best_asteroid)

  laser_angle_asteroids = set()
  for asteroid in asteroids.keys():
    if asteroid != best_asteroid:
      angle = math.atan2(best_asteroid[1] - asteroid[1], best_asteroid[0] - asteroid[0])
      dist = ((best_asteroid[1] - asteroid[1]) ** 2.0 + (best_asteroid[0] - asteroid[0]) ** 2.0) ** 0.5
      laser_angle_asteroids.add((round(scale * angle), round(scale * dist), *asteroid))
  laser_angle_asteroids = list(sorted(laser_angle_asteroids))

  index = 0
  for start_index in itertools.count():
    if laser_angle_asteroids[start_index][0] >= round(scale * math.pi / 2):
      index = start_index
      break

  destroy_order = {}
  destroyed = 0
  while laser_angle_asteroids:
    destroy = laser_angle_asteroids.pop(index)
    angle = destroy[0]
    destroyed += 1
    if destroyed == 200:
      print("destroyed", destroyed, destroy[2:])
      print("answer", 100 * destroy[2] + destroy[3])
    if laser_angle_asteroids:
      if index == len(laser_angle_asteroids):
        index = 0
      else:
        while laser_angle_asteroids[index][0] == angle:
          index += 1
          if index >= len(laser_angle_asteroids):
            index = 0
            break
