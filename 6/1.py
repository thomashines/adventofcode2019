#!/usr/bin/env python3

import sys

def fill_indirects(objects, root):
  for orbiter in objects[root]["directly_orbited_by"]:
    fill_indirects(objects, orbiter)
    objects[orbiter]["indirectly_orbits"].add(root)
    objects[orbiter]["orbits"].add(root)
    objects[root]["indirectly_orbited_by"].update(objects[orbiter]["directly_orbited_by"])
    objects[root]["indirectly_orbited_by"].update(objects[orbiter]["indirectly_orbited_by"])
    objects[root]["orbited_by"].update(objects[root]["indirectly_orbited_by"])

def orbit_count(objects, root):
  orbits = len(objects[root]["directly_orbited_by"]) + len(objects[root]["indirectly_orbited_by"])
  for orbiter in objects[root]["directly_orbited_by"]:
    orbits += orbit_count(objects, orbiter)
  return orbits

def search(objects, start, end):
  length = 0

  root = start
  while end not in objects[root]["orbited_by"]:
    root = objects[root]["directly_orbits"]
    if root is None:
      return None
    length += 1

  root = end
  while start not in objects[root]["orbited_by"]:
    root = objects[root]["directly_orbits"]
    if root is None:
      return None
    length += 1

  return length - 2

if __name__ == "__main__":
  objects = {}
  for orbited, orbiter in (line.strip().split(")") for line in sys.stdin):
    for object in (orbited, orbiter):
      if object not in objects:
        objects[object] = {
          "directly_orbited_by": set(),
          "directly_orbits": None,
          "indirectly_orbited_by": set(),
          "indirectly_orbits": set(),
          "orbited_by": set(),
          "orbits": set(),
        }
    objects[orbited]["directly_orbited_by"].add(orbiter)
    objects[orbiter]["directly_orbits"] = orbited
    objects[orbited]["orbited_by"].add(orbiter)
    objects[orbiter]["orbits"].add(orbited)

  fill_indirects(objects, "COM")
  print("orbit_count", orbit_count(objects, "COM"))
  print("search", search(objects, "YOU", "SAN"))
