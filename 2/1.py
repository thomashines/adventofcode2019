#!/usr/bin/env python3

import itertools
import sys

if __name__ == "__main__":
  prog_string = ",".join(line.strip() for line in sys.stdin)
  # print(prog_string)
  orig_prog = {key: value for key, value in zip(itertools.count(), (int(val) for val in prog_string.split(",")))}
  for noun in range(100):
    for verb in range(100):
      prog = {key: value for key, value in orig_prog.items()}
      prog[1] = noun
      prog[2] = verb
      index = 0
      while index in prog:
        opcode = prog[index]
        if opcode == 1:
          prog[prog.get(index + 3, 0)] = prog.get(prog.get(index + 1, 0), 0) + prog.get(prog.get(index + 2, 0), 0)
        elif opcode == 2:
          prog[prog.get(index + 3, 0)] = prog.get(prog.get(index + 1, 0), 0) * prog.get(prog.get(index + 2, 0), 0)
        elif opcode == 99:
          break
        index += 4
      if (noun, verb) == (12, 2) or prog[0] == 19690720:
        print(noun, verb, prog[0], 100 * noun + verb)
  # done = []
  # for index in sorted(prog.keys()):
  #   done.append(str(prog[index]))
  # print(",".join(done))
