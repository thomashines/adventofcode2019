#!/usr/bin/env python3

import heapq
import itertools
import sys

def l1_norm(start, goal):
  return abs(goal[0] - start[0]) + abs(goal[1] - start[1])

def main():
  map = {
    "floors": set(),
    "portal_characters": {},
    "portals": {},
    "known": set(),
  }
  y = 0
  for line in sys.stdin:
    x = 0
    for character in line:
      if character == ".":
        map["floors"].add((x, y))
      if ord("A") <= ord(character) <= ord("Z"):
        map["portal_characters"][x, y] = character
      map["known"].add((x, y))
      x += 1
    y += 1

  for (x, y), character in map["portal_characters"].items():
    other_character = None
    other_character_position = None
    floor_position = None
    for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
      position = (x + dx, y + dy)
      if position in map["floors"]:
        floor_position = position
      if position in map["portal_characters"]:
        other_character = map["portal_characters"][position]
        other_character_position = position
    if floor_position is not None and other_character_position is not None:
      is_inside = (x - 3 * (floor_position[0] - x),
                   y - 3 * (floor_position[1] - y)) in map["known"]
      portal_key = (character + other_character) if ((x, y) < other_character_position) else (other_character + character)
      if portal_key not in map["portals"]:
        map["portals"][portal_key] = {
          "ends": [],
          "inside": None,
          "outside": None,
        }
      if is_inside:
        map["portals"][portal_key]["inside"] = floor_position
      else:
        map["portals"][portal_key]["outside"] = floor_position
      map["portals"][portal_key]["ends"].append(floor_position)

  # Just do Dijkstras
  start = map["portals"]["AA"]["ends"][0]
  goal = map["portals"]["ZZ"]["ends"][0]

  visited = set()

  open = []
  open.append((start, 0))
  while open:
    position, steps = open.pop(0)
    if position == goal:
      print("done", steps)
      break
    if position in visited:
      continue
    visited.add(position)
    for portal_key, portal in map["portals"].items():
      if position in portal["ends"] and len(portal["ends"]) == 2:
        other_end = portal["ends"][1 - portal["ends"].index(position)]
        if other_end not in visited:
          open.append((other_end, steps + 1))
    for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
      next_position = (position[0] + dx, position[1] + dy)
      if next_position in map["floors"] and next_position not in visited:
        open.append((next_position, steps + 1))

  visited = set()

  index = 0

  open = []
  heapq.heappush(open, (0, index, start, 0))
  index += 1
  while open:
    level, _, position, steps = heapq.heappop(open)
    if level == 0 and position == goal:
      print("done", steps)
      break
    if (level, position) in visited:
      continue
    visited.add((level, position))
    for portal_key, portal in map["portals"].items():
      if len(portal["ends"]) == 1:
        continue
      if position == portal["inside"]:
        if (level + 1, portal["outside"]) not in visited:
          heapq.heappush(open, (level + 1, index, portal["outside"], steps + 1))
          index += 1
      if level >= 1 and position == portal["outside"]:
        if (level - 1, portal["inside"]) not in visited:
          heapq.heappush(open, (level - 1, index, portal["inside"], steps + 1))
          index += 1
    for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
      next_position = (position[0] + dx, position[1] + dy)
      if next_position in map["floors"] and (level, next_position) not in visited:
        heapq.heappush(open, (level, index, next_position, steps + 1))
        index += 1

if __name__ == "__main__":
  main()
